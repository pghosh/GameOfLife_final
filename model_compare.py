import matplotlib.pyplot as plt
import numpy as np

filename = ["3k/out.txt"]
mydict = {}
flag = 0

for x in filename:
	fp = open(x,"r")
	lines = fp.readlines()
	for i in range(0,len(lines)):
		if("Time" in lines[i]):
			if("Sync" in lines[i]):
				if("Sync" in mydict):
					mydict["Sync"] += float(lines[i+1])
				else:
					mydict["Sync"] = float(lines[i+1])
			elif("Sim" in lines[i]):
				if("Sim" in mydict):
					mydict["Sim"] += float(lines[i+1])
				else:
					mydict["Sim"] = float(lines[i+1])
			elif("Memcpy" in lines[i]):
				if("Memcpy" in mydict):
					mydict["Memcpy"] += float(lines[i+1])
				else:
					mydict["Memcpy"] = float(lines[i+1])
			elif("DP" in lines[i]):
				if(("DP"+str(lines[i][-3:-1])) in mydict):
					mydict["DP"+str(lines[i][-3:-1])] += float(lines[i+1])
				else:
					mydict["DP"+str(lines[i][-3:-1])] = float(lines[i+1])

for x in mydict:
	mydict[x] = mydict[x]/len(filename);

mykeys = ["DP01","DP05","DP10","DP15","DP20","DP25","DP30","DP35","DP40","DP45","DP50","DP55","DP60","DP65","DP70","DP75","DP80","Memcpy","Sync"]
myvalues = []
for x in mykeys:
	myvalues.append(mydict[x])

index = np.arange(len(mykeys))
plt.bar(index,myvalues)
plt.xticks(index,mykeys,fontsize = 12,rotation = 30)
plt.ylabel("Average Time", fontsize = 15)
plt.show()