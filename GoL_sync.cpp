# define _BSD_SOURCE
# include <chrono>
# include <thread>
# include <sys/time.h>
# include <cstdio>
# include <iostream>
# include <vector>
# include <fstream>
# include <limits>
# include <iomanip>
# include "tbb/parallel_for.h"
# include "tbb/parallel_reduce.h"
# include "tbb/blocked_range2d.h"
# include "tbb/blocked_range.h"
# include "tbb/tick_count.h"
# include "tbb/flow_graph.h"
# include "tbb/enumerable_thread_specific.h"
# include "tbb/task_scheduler_init.h"

using namespace std;
using namespace tbb;

class Alive{
	bool** arr;
	size_t size;
public:
	double my_sum;
	void operator()(const blocked_range<size_t>& r){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics Task start");
		my_counter.timestamp.push_back(tick_count::now());*/
		bool** a = arr;
		double sum = my_sum;
		for(size_t i=r.begin();i<r.end();++i){
			for(size_t j=1;j<(size-1);++j){
				if(a[i][j]==1){
					sum ++;
				} 
			}
		}
		my_sum = sum;
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics Task end");
		my_counter.index += 2;*/
	}

	Alive(Alive& x,split):arr(x.arr),size(x.size),my_sum(0){}

	void join(const Alive& y){my_sum += y.my_sum;}

	Alive(bool** a,size_t b):arr(a),size(b),my_sum(0){}
};

class MyNeighbourhood{
	bool** arr;
	size_t size;
	int myNeighbours(size_t xpos,size_t ypos,bool** myarr){
		return (myarr[xpos+1][ypos] + myarr[xpos][ypos+1] + myarr[xpos-1][ypos] + myarr[xpos][ypos-1] + myarr[xpos+1][ypos+1] + myarr[xpos-1][ypos-1] + myarr[xpos+1][ypos-1] + myarr[xpos-1][ypos+1]);
	}
public:
	int tarr[9];
	void operator()(const blocked_range<size_t>& r){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics Task start");
		my_counter.timestamp.push_back(tick_count::now());*/
		bool** a = arr;
		for(size_t i=r.begin();i<r.end();++i){
			for(size_t j=1;j<(size-1);++j){
				tarr[myNeighbours(i,j,a)] += 1;
			}
		}
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics Task end");
		my_counter.index += 2;*/
	}

	MyNeighbourhood(MyNeighbourhood& x,split):arr(x.arr),size(x.size){
		for(size_t i=0;i<=9;++i){
			tarr[i]=0;
		}
	}

	void join(const MyNeighbourhood& y){
		for(size_t i=0;i<=9;++i){
			tarr[i] += y.tarr[i];
		}
	}

	MyNeighbourhood(bool** a,size_t x):arr(a),size(x){
		for(int i=0;i<=9;++i){
			tarr[i]=0;
		}
	}
};

struct GameOfLife{
private:
	bool** oldbuf;
	bool** newbuf;
	size_t size;
	ofstream visfile;

	inline void ParticleUpdate(int x, int y){
		int liveNeighbours = this->oldbuf[x-1][y-1] + this->oldbuf[x-1][y] + this->oldbuf[x-1][y+1] + this->oldbuf[x][y-1] 
							+ this->oldbuf[x][y+1] + this->oldbuf[x][y+1] + this->oldbuf[x+1][y-1] 
							+ this->oldbuf[x+1][y] + this->oldbuf[x+1][y+1];
		if(this->oldbuf[x][y]){
			if(liveNeighbours == 2 || liveNeighbours == 3){
				this->newbuf[x][y] = true;
			}
			else{
				this->newbuf[x][y] = false;
			}
		}
		else{
			if(liveNeighbours == 3){
				this->newbuf[x][y] = true;
			}
			else{
				this->newbuf[x][y] = false;
			}
		}
	}
public:
	void init(size_t mysize){
		srand(time(NULL));
		this->size = mysize;
		oldbuf = new bool*[mysize];
		newbuf = new bool*[mysize];
		for(size_t i=0;i<mysize;++i){
			oldbuf[i]=new bool[mysize];
			newbuf[i]=new bool[mysize];
			for(size_t j=0;j<mysize;++j){
				oldbuf[i][j]=rand()%2;
				newbuf[i][j]=rand()%2;
			}
		}
		visfile.open("visual.txt",std::ios::out | std::ios::trunc);
	}

	void simulator(){
		affinity_partitioner ap;
		parallel_for(blocked_range<size_t>(1,(this->size)-1),
			[=](const blocked_range<size_t>& r){
				for(size_t i = r.begin(); i!=r.end(); ++i){
					for(size_t j = 1; j<(this->size)-1; ++j){
						ParticleUpdate(i,j);
					}
				}
			}
			,ap);
	}

	void live_cells(){
		affinity_partitioner ap;
		Alive lobj(this->newbuf,this->size);
		parallel_reduce(blocked_range<size_t>(1,(this->size)-1),lobj,ap);
	}

	void neighbourhood(){
		affinity_partitioner ap;
		MyNeighbourhood nobj(this->newbuf,this->size);
		parallel_reduce(blocked_range<size_t>(1,(this->size)-1),nobj,ap);
	}

	void visualize(){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics start");
		my_counter.timestamp.push_back(tick_count::now());*/
		for(size_t i=1;i<(this->size)-1;++i){
			for(size_t j=1;j<(this->size-1);++j){
				if(this->newbuf[i][j]==1){
					visfile<<".";
				}
				else{
					visfile<<" ";
				}
			}
			visfile<<endl;
		}
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics end");
		my_counter.index += 2;*/
	}

	void update(){
		swap(oldbuf,newbuf);
	}
};

GameOfLife* GoL;

int main(int argc,char* argv[]){
	size_t mysize = atoi(argv[1]);
	size_t iterations = 20;
	size_t vis_frequency = 5;
	size_t vis_cycles = 2;
	size_t vis_offset = 0;
	size_t live_frequency = 2;
	size_t live_cycles = 10;
	size_t live_offset = 1;
	size_t neigh_frequency = 2;
	size_t neigh_cycles = 10;
	size_t neigh_offset = 1;
	double time_taken = 0.0;

	GoL = new GameOfLife;
	GoL->init(mysize);

	tick_count t0 = tick_count::now();
	for(size_t i=0;i<iterations;++i){
		GoL->simulator();
		/*if((i+vis_offset)%(vis_frequency)==0){
			GoL->visualize();
		}*/
		/*if((i+live_offset)%live_frequency == 0){
			GoL->live_cells();
		}
		if((i+neigh_offset)%neigh_frequency == 0){
			GoL->neighbourhood();
		}*/
		GoL->update();
	}
	tick_count t1 = tick_count::now();
	time_taken += (t1-t0).seconds();

	cout<<time_taken<<endl;
}
