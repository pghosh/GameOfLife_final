#!/bin/bash

lscpu > datai.txt
. ../../tbb/tbb43_20150611oss/bin/tbbvars.sh intel64

# g++ GoL_sync.cpp -ltbb -std=c++11
# echo "Sync Time" >> out.txt
# ./a.out $1 >> out.txt
# echo "Sync Done"

# g++ GoL_memcpy.cpp -ltbb -std=c++11
# echo "Memcpy Time" >> out.txt
# ./a.out $1 >> out.txt
# echo "Memcpy Done"

g++ GoL_graph_dp.cpp -ltbb -std=c++11
for number in 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80
do 
	echo "DP_Time" $number >> outi.txt
	perf stat -o out_$number.txt -r 3 -e instructions,cpu-cycles,cache-misses,cache-references,LLC-load-misses,LLC-loads,LLC-store-misses ./a.out $1 $number >> outi.txt
	echo "DONE"
done

exit 0
