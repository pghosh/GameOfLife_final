import matplotlib.pyplot as plt
from ggplot import *
import numpy as np 
import pandas as pd 
from pylab import figure, show, legend, ylabel, xlabel


filepath = "40k/out_"
blocks = [2,4,6,8,10,12,14,16,18]
mydict = {}
mylist = []
four_val = [88.31183333333333, 87.5775, 87.67626666666666, 87.87819999999999, 88.03596666666668, 88.10483333333333, 88.27163333333334, 88.44260000000001, 88.4526]
five_val = [137.93733333333333, 136.83966666666666, 137.067, 137.28900000000002, 137.5823333333333, 138.0836666666667, 137.75300000000001, 137.904, 137.96]


for x in blocks:
	fp = open(filepath+str(x)+".txt","r")
	mydict["Blocks"] = x
	lines = fp.readlines()
	for i in range(0,len(lines)):
		if("cache-misses" in lines[i]):
			mydict["Cache_miss_percentage"] = float(lines[i].split(" ")[30])
		if("LLC-load-misses" in lines[i]):
			mydict["LLC_miss_percentage"] = float(lines[i].split(" ")[-16][:-1])
	mydict["Time"] = four_val[(x//2)-1]
	mylist.append(mydict.copy())
	mydict.clear()

dat = pd.DataFrame(mylist)
print(dat)
'''
print(
	ggplot(dat,aes(x='Blocks',y='Cache_miss_percentage')) +\
	geom_line(color='red',linetype='solid',size='1') +\
	geom_point(color='black',size=10,shape=10,fill='red') +\
	geom_vline(color='red',x=[5,10],linetype='dashed') +\
	scale_x_continuous(breaks=blocks) +\
	xlim(0,25) +\
 	ylim(0,100) +\
	xlab("Blocks") +\
	ylab("L3 cache_miss percentage") +\
	ggtitle("Cache Performance")
 	)
'''

cache_miss = []
time = []
for i in range(0,len(blocks)):
	cache_miss.append(dat.ix[i]["Cache_miss_percentage"])
	time.append(dat.ix[i]["Time"])

'''
plt.plot(blocks,time,'or-')
plt.xticks(blocks,blocks,fontsize = 12,rotation = 30)
plt.ylabel("Average Time", fontsize = 15)
plt.xlabel("Blocks in each dimension", fontsize = 15)
plt.show()
'''

fig1 = figure()
 
# and the first axes using subplot populated with data 
ax1 = fig1.add_subplot(111)
line1 = ax1.plot(blocks,cache_miss, 'o-',label="Cache Miss percentage")
ax1.set_ylim([0,100])
ylabel("Cache Miss Percentage")
 
# now, the second axes that shares the x-axis with the ax1
ax2 = fig1.add_subplot(111, sharex=ax1, frameon=False)
line2 = ax2.plot(blocks,time, 'xr-',label="Execution Time")
ax2.yaxis.tick_right()
ax2.yaxis.set_label_position("right")
ylabel("Execution Time")
xlabel("Blocks in each dimension")
 
#legend((line1, line2), ("abc", "def"), loc = 9)
fig1.legend(loc=9)
show()