# started on Fri May  4 15:51:42 2018


 Performance counter stats for './a.out 40002 18' (4 runs):

 5,975,069,026,548      instructions              #    2.56  insns per cycle          ( +-  0.00% )  (42.85%)
 2,330,040,923,462      cpu-cycles                                                    ( +-  0.01% )  (57.14%)
       123,627,382      cache-misses              #   21.735 % of all cache refs      ( +-  0.31% )  (71.42%)
       568,791,529      cache-references                                              ( +-  0.41% )  (85.71%)
        67,096,138      LLC-load-misses           #   27.79% of all LL-cache hits     ( +-  0.37% )  (85.72%)
       241,454,942      LLC-loads                                                     ( +-  0.97% )  (85.72%)
        55,884,943      LLC-store-misses                                              ( +-  0.33% )  (28.56%)

     134.215008559 seconds time elapsed                                          ( +-  0.02% )

