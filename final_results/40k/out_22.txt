# started on Fri May  4 16:09:36 2018


 Performance counter stats for './a.out 40002 22' (4 runs):

 5,983,578,077,618      instructions              #    2.56  insns per cycle          ( +-  0.00% )  (42.86%)
 2,338,130,176,734      cpu-cycles                                                    ( +-  0.03% )  (57.14%)
       108,334,492      cache-misses              #   16.751 % of all cache refs      ( +-  0.32% )  (71.43%)
       646,744,752      cache-references                                              ( +-  0.75% )  (85.72%)
        57,629,776      LLC-load-misses           #   20.61% of all LL-cache hits     ( +-  0.58% )  (85.71%)
       279,660,170      LLC-loads                                                     ( +-  1.29% )  (85.71%)
        50,071,982      LLC-store-misses                                              ( +-  0.22% )  (28.57%)

     134.511580577 seconds time elapsed                                          ( +-  0.03% )

