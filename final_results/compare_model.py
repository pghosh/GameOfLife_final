import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np

filename = ["../final_resultsII/50k/out.txt"]
mydict = {}

bar_graph = True
line_graph = False

for x in filename:
	fp = open(x,"r")
	lines = fp.readlines()
	for i in range(0,len(lines)):
			if("DP" in lines[i]):
				mydict[str(lines[i][-3:-1])] = (float(lines[i+1]) + float(lines[i+2]) + float(lines[i+3]))/3.0
key_min = min(mydict.keys(), key=(lambda k: mydict[k]))

for x in filename:
	fp = open(x,"r")
	lines = fp.readlines()
	for i in range(0,len(lines)):
			if("Sim" in lines[i]):
				mydict["Sim"] = float(lines[i+1])
			if("Sync" in lines[i]):
				mydict["Sync"] = float(lines[i+1])


mykeys = ["Sim"," 1"," 2"," 3"," 4"," 5"," 6"," 7"," 8"," 9","10","11","12","13","14","15","16","Sync"]#,"DP17","DP18","DP19","DP20","Sync"]

if(bar_graph):
	myvalues = []
	for x in mykeys:
		myvalues.append(mydict[x])

	index = np.arange(len(mykeys))

	barlist = plt.bar(index,myvalues,align='center', alpha = 0.5,color = (0,1,0,0.9))
	barlist[0].set_color('blue')
	barlist[-1].set_color((1,0,0,0.8))
	plt.plot(index,np.full((len(mykeys),),mydict["Sim"]),'r--',linewidth=1.0)
	plt.xticks(index,mykeys,fontsize = 12,rotation = 30)
	plt.ylabel("Average Time", fontsize = 15)
	plt.xlabel("Blocks in each dimension", fontsize = 15)
	plt.legend((barlist[0], barlist[1], barlist[-1]), ('Simulation Time', 'Blocking Model', 'Synchronous Execution Time'), loc = 9)
	plt.show()
	#plt.plot(index,np.full((len(mykeys),),mydict[key_min]),'r--',linewidth=1.0)

if(line_graph):
	mykeys = mykeys[1:-1]
	myvalues = []
	for x in mykeys:
		myvalues.append(mydict[x])

	index = np.arange(len(mykeys))

	plt.plot(index,myvalues,'or-')
	plt.plot(index,np.full((len(mykeys),),mydict[key_min]),'r--',linewidth=1.0)
	plt.xticks(index,mykeys,fontsize = 12,rotation = 30)
	plt.ylabel("Average Time", fontsize = 15)
	plt.xlabel("Blocks in each dimension", fontsize = 15)
	plt.show()