# define _BSD_SOURCE
# include <chrono>
# include <thread>
# include <sys/time.h>
# include <cstdio>
# include <iostream>
# include <vector>
# include <fstream>
# include <limits>
# include <iomanip>
# include "tbb/parallel_for.h"
# include "tbb/parallel_reduce.h"
# include "tbb/blocked_range2d.h"
# include "tbb/blocked_range.h"
# include "tbb/tick_count.h"
# include "tbb/flow_graph.h"
# include "tbb/enumerable_thread_specific.h"
# include "tbb/task_scheduler_init.h"

using namespace std;
using namespace tbb;
using namespace tbb::flow;

#define ll double

bool block_analytics = true;
bool block_sim_analytics = true;

/*
Color Codes:
	Analytics: Cyan->Blue
	Simulation: Light Green -> Green
	Initialization: White
	Joins: Yellow
	Idle: Red
*/

/*struct Traces{
	size_t index;
	tick_count start;
	vector<tick_count> timestamp;
	vector<string> name;
	Traces(size_t x,tick_count y):index(x),start(y){}
};*/

/*typedef enumerable_thread_specific<Traces> TLS;
TLS MyCounter(Traces(0,tick_count::now()));*/

class Alive{
	bool** arr;
	size_t start;
	size_t end;
public:
	double my_sum;
	void operator()(const blocked_range<size_t>& r){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics Task start");
		my_counter.timestamp.push_back(tick_count::now());*/
		bool** a = arr;
		double sum = my_sum;
		for(size_t i=r.begin();i<r.end();++i){
			for(size_t j=start;j<end;++j){
				if(a[i][j]){
					sum ++;
				} 
			}
		}
		my_sum = sum;
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics Task end");
		my_counter.index += 2;*/
	}

	Alive(Alive& x,split):arr(x.arr),start(x.start),end(x.end),my_sum(0){}

	void join(const Alive& y){my_sum += y.my_sum;}

	Alive(bool** a,size_t b,size_t c, double d):arr(a),start(b),end(c),my_sum(d){}
};

class Alive2d{
	bool** arr;
public:
	double my_sum;
	void operator()(const blocked_range2d<size_t>& r){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics Task start");
		my_counter.timestamp.push_back(tick_count::now());*/
		bool** a = arr;
		double sum = my_sum;
		for(size_t i=r.rows().begin(); i!=r.rows().end(); ++i){
			for(size_t j=r.cols().begin(); j!=r.cols().end(); ++j){
				if(a[i][j]){
					sum ++;
				} 
			}
		}
		my_sum = sum;
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics Task end");
		my_counter.index += 2;*/
	}

	Alive2d(Alive2d& x,split):arr(x.arr),my_sum(0){}

	void join(const Alive2d& y){my_sum += y.my_sum;}

	Alive2d(bool** a, double b):arr(a),my_sum(b){}
};

class MyNeighbourhood{
	bool** arr;
	size_t start;
	size_t end;
	int myNeighbours(size_t xpos,size_t ypos,bool** myarr){
		return (myarr[xpos+1][ypos] + myarr[xpos][ypos+1] + myarr[xpos-1][ypos] + myarr[xpos][ypos-1] + myarr[xpos+1][ypos+1] + myarr[xpos-1][ypos-1] + myarr[xpos+1][ypos-1] + myarr[xpos-1][ypos+1]);
	}
public:
	int tarr[9];
	void operator()(const blocked_range<size_t>& r){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics Task start");
		my_counter.timestamp.push_back(tick_count::now());*/
		bool** a = arr;
		for(size_t i=r.begin();i<r.end();++i){
			for(size_t j=start; j<end; ++j){
				tarr[myNeighbours(i,j,a)] += 1;
			}
		}
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics Task end");
		my_counter.index += 2;*/
	}

	MyNeighbourhood(MyNeighbourhood& x,split):arr(x.arr),start(x.start),end(x.end){
		for(size_t i=0; i<=9; ++i){
			tarr[i]=0;
		}
	}

	void join(const MyNeighbourhood& y){
		for(size_t i=0; i<=9; ++i){
			tarr[i] += y.tarr[i];
		}
	}

	MyNeighbourhood(bool** a,size_t x,size_t y):arr(a),start(x),end(y){
		for(int i=0; i<=9; ++i){
			tarr[i]=0;
		}
	}
};

class MyNeighbourhood2d{
	bool** arr;
	int myNeighbours(size_t xpos,size_t ypos,bool** myarr){
		return (myarr[xpos+1][ypos] + myarr[xpos][ypos+1] + myarr[xpos-1][ypos] + myarr[xpos][ypos-1] + myarr[xpos+1][ypos+1] + myarr[xpos-1][ypos-1] + myarr[xpos+1][ypos-1] + myarr[xpos-1][ypos+1]);
	}
public:
	int tarr[9];
	void operator()(const blocked_range2d<size_t>& r){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics Task start");
		my_counter.timestamp.push_back(tick_count::now());*/
		bool** a = arr;
		for(size_t i=r.rows().begin(); i<r.rows().end();++i){
			for(size_t j=r.cols().begin(); j!=r.cols().end(); ++j){
				tarr[myNeighbours(i,j,a)] += 1;
			}
		}
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics Task end");
		my_counter.index += 2;*/
	}

	MyNeighbourhood2d(MyNeighbourhood2d& x,split):arr(x.arr){
		for(size_t i=0;i<=9;++i){
			tarr[i]=0;
		}
	}

	void join(const MyNeighbourhood2d& y){
		for(size_t i=0;i<=9;++i){
			tarr[i] += y.tarr[i];
		}
	}

	MyNeighbourhood2d(bool** a):arr(a){
		for(size_t i=0;i<=9;++i){
			tarr[i]=0;
		}
	}
};

struct GameOfLife{
private:
	bool** oldbuf;
	bool** newbuf;
	size_t size;
	size_t blocks;
	int total_blocks;
	int tot_compute_blocks;
	int block_size;
	vector<pair<pair<size_t,size_t>,pair<size_t,size_t> > > block_points;
	vector<pair<pair<size_t,size_t>,pair<size_t,size_t> > > compute_blocks;
	ofstream visfile;
	ofstream datafile;
	ll alive_count;

	inline void ParticleSimulate(int xpos,int ypos){
		int temp = this->newbuf[xpos][ypos];
		if(this->oldbuf[xpos][ypos] == 1){
			if(temp<2 || temp>3){
				this->newbuf[xpos][ypos]=0;
			}
			else{
				this->newbuf[xpos][ypos]=1;
			}	
		}
		else{
			if(temp==3){
				this->newbuf[xpos][ypos]=1;
			}
			else{
				this->newbuf[xpos][ypos]=0;
			}
		}
	}

	inline void ParticleUpdate(int x, int y){
		int liveNeighbours = this->oldbuf[x-1][y-1] + this->oldbuf[x-1][y] + this->oldbuf[x-1][y+1] + this->oldbuf[x][y-1] 
							+ this->oldbuf[x][y+1] + this->oldbuf[x][y+1] + this->oldbuf[x+1][y-1] 
							+ this->oldbuf[x+1][y] + this->oldbuf[x+1][y+1];
		if(this->oldbuf[x][y]){
			if(liveNeighbours == 2 || liveNeighbours == 3){
				this->newbuf[x][y] = true;
			}
			else{
				this->newbuf[x][y] = false;
			}
		}
		else{
			if(liveNeighbours == 3){
				this->newbuf[x][y] = true;
			}
			else{
				this->newbuf[x][y] = false;
			}
		}
	}

	inline void ParticleUpdateAll(int x, int y){
		int liveNeighbours = this->oldbuf[x-1][y-1] + this->oldbuf[x-1][y] + this->oldbuf[x-1][y+1] + this->oldbuf[x][y-1] 
							+ this->oldbuf[x][y+1] + this->oldbuf[x][y+1] + this->oldbuf[x+1][y-1] 
							+ this->oldbuf[x+1][y] + this->oldbuf[x+1][y+1];
		if(this->oldbuf[x][y]){
			if(liveNeighbours == 2 || liveNeighbours == 3){
				this->newbuf[x][y] = true;
			}
			else{
				this->newbuf[x][y] = false;
			}
		}
		else{
			if(liveNeighbours == 3){
				this->newbuf[x][y] = true;
			}
			else{
				this->newbuf[x][y] = false;
			}
		}
		int xblock = x/(this->block_size);
		int yblock = y/(this->block_size);
		int comp_block_id = (xblock*(this->tot_compute_blocks)) + yblock; 
		int xstart,xend,ystart,yend;
		xstart = this->compute_blocks[comp_block_id].first.first;
		xend = this->compute_blocks[comp_block_id].first.second;
		ystart = this->compute_blocks[comp_block_id].second.first;
		yend = this->compute_blocks[comp_block_id].second.second;
		int temp = 0;
		for(size_t a=xstart; a < xend; ++a){
			for(size_t b= ystart; b < yend; ++b){
				temp += this->oldbuf[a][b];
			}
		}
	}
public:
	GameOfLife(){}

	void init(size_t mysize, size_t myblocks){
		srand(time(NULL));
		this->size = mysize;
		this->blocks = myblocks;
		visfile.open("visual.txt",std::ios::out | std::ios::trunc);
		datafile.open("track.txt",std::ios::out );
		oldbuf = new bool*[mysize];
		newbuf = new bool*[mysize];
		for(size_t i=0;i<mysize;++i){
			oldbuf[i]=new bool[mysize];
			newbuf[i]=new bool[mysize];
			for(size_t j=0;j<mysize;++j){
				oldbuf[i][j]=rand()%2;
				newbuf[i][j]=rand()%2;
			}
		}
		size_t tstart,tend;
		vector<pair<size_t,size_t> > t_points;
		for(size_t j = 0;j<myblocks; ++j){
			tstart = ((mysize/myblocks)*j)+1;
			tend = ((mysize/myblocks)*(j+1))+1;
			if(j==(myblocks-1)){
				tend = mysize-1;
			}
			t_points.push_back(make_pair(tstart,tend));
		}
		this->total_blocks = 0;
		int xend,xstart,ystart,yend;
		for(size_t j = 0; j<myblocks; ++j){
			xstart = t_points[j].first; 
			xend = t_points[j].second;
			for(size_t i = 0; i<myblocks; ++i){
				ystart = t_points[i].first;
				yend = t_points[i].second;
				this->total_blocks += 1;
				block_points.push_back(make_pair(make_pair(xstart,xend),make_pair(ystart,yend)));
				//cout<<xstart<<":"<<xend<<" "<<ystart<<":"<<yend<<endl;
			}
		}
		alive_count = 0;
		/*
		******************************
		Code to calculate the compute blocks to see effect of cache on the code
		******************************
		*/
		for(size_t j = 0;j<tot_compute_blocks; ++j){
			tstart = ((mysize/tot_compute_blocks)*j)+1;
			tend = ((mysize/tot_compute_blocks)*(j+1))+1;
			if(j==(myblocks-1)){
				tend = mysize-1;
			}
			t_points.push_back(make_pair(tstart,tend));
		}
		this->tot_compute_blocks = 200;
		this->block_size = (mysize/(this->tot_compute_blocks));
		for(size_t j = 0; j<tot_compute_blocks; ++j){
			xstart = t_points[j].first; 
			xend = t_points[j].second;
			for(size_t i = 0; i<tot_compute_blocks; ++i){
				ystart = t_points[i].first;
				yend = t_points[i].second;
				compute_blocks.push_back(make_pair(make_pair(xstart,xend),make_pair(ystart,yend)));
			}
		}
	}

	void block_simulator(size_t block_id, int grain = 1){
		affinity_partitioner ap;
		size_t start = this->block_points[block_id].second.first; 
		size_t end = this->block_points[block_id].second.second;
		parallel_for(blocked_range<size_t>(this->block_points[block_id].first.first,this->block_points[block_id].first.second,grain),
			[=](const blocked_range<size_t>& r){
				for(size_t i = r.begin(); i!=r.end(); ++i){
					for(size_t j = start; j<end; ++j){
						//ParticleUpdate(i,j);
						ParticleUpdateAll(i,j);
					}
				}
			}
			,ap);
	}

	void block_simulator_2d(size_t block_id, int grain = 1){
		affinity_partitioner ap;
		size_t start = this->block_points[block_id].second.first; 
		size_t end = this->block_points[block_id].second.second;
		parallel_for(blocked_range2d<size_t>(this->block_points[block_id].first.first,this->block_points[block_id].first.second,grain,start,end,grain),
			[=](const blocked_range2d<size_t>& r){
				for(size_t i = r.rows().begin(); i!=r.rows().end(); ++i){
					for(size_t j = r.cols().begin(); j!=r.cols().end(); ++j){
						ParticleUpdate(i,j);
					}
				}
			}
			,ap);
	}

	void block_visualize(size_t block_id){
		for(size_t i=(this->block_points[block_id].first.first);i<(this->block_points[block_id].first.second);++i){
			for(size_t j=(this->block_points[block_id].second.first);j<(this->block_points[block_id].second.second);++j){
				if(this->newbuf[i][j]==1){
					visfile<<".";
				}
				else{
					visfile<<" ";
				}
			}
			visfile<<endl;
		}
	}

	void live_cells(size_t block_id){
		affinity_partitioner ap;
		Alive obj(this->oldbuf,this->block_points[block_id].second.first,this->block_points[block_id].second.first,this->alive_count);
		parallel_reduce(blocked_range<size_t>(this->block_points[block_id].first.first,this->block_points[block_id].first.second),obj,ap);
		alive_count = obj.my_sum;
	}

	void live_cells_2d(size_t block_id, int grain=1){
		affinity_partitioner ap;
		Alive2d obj(this->newbuf,this->alive_count);
		parallel_reduce(blocked_range2d<size_t>(this->block_points[block_id].first.first,this->block_points[block_id].first.second,grain,
			this->block_points[block_id].second.second,this->block_points[block_id].second.second,grain),obj,ap);
		alive_count = obj.my_sum;
	}

	void neighbourhood(size_t block_id){
		affinity_partitioner ap;
		MyNeighbourhood obj(this->oldbuf,this->block_points[block_id].second.first,this->block_points[block_id].second.first);
		parallel_reduce(blocked_range<size_t>(this->block_points[block_id].first.first,this->block_points[block_id].first.second),obj,ap);
	}

	void neighbourhood_2d(size_t block_id, int grain=1){
		affinity_partitioner ap;
		MyNeighbourhood2d obj(this->newbuf);
		parallel_reduce(blocked_range2d<size_t>(this->block_points[block_id].first.first,this->block_points[block_id].first.second,grain,
			this->block_points[block_id].second.second,this->block_points[block_id].second.second,grain),obj,ap);
	}

	void update(){
		swap(oldbuf,newbuf);
	}

	int getblocks(){
		return this->total_blocks;
	}
};

GameOfLife* GoL;

struct sim_body{
	size_t id;
	sim_body(size_t x):id(x){}
	void operator()(continue_msg)const{
		GoL->block_simulator(id);
	}
};

struct vis_body{
	size_t id;
	vis_body(size_t x):id(x){}
	void operator()(continue_msg)const{
		GoL->block_visualize(id);
	}
};

struct live_body{
	size_t id;
	live_body(size_t x):id(x){}
	void operator()(continue_msg)const{
		GoL->live_cells(id);
	}
};

struct neighbour_body{
	size_t id;
	neighbour_body(size_t x):id(x){}
	void operator()(continue_msg)const{
		GoL->neighbourhood(id);
	}
};

struct update_body{
	update_body(){}
	void operator()(continue_msg)const{
		GoL->update();
	}
};

int main(int argc,char* argv[]){
	/*TLS::reference my_counter = MyCounter.local();
	my_counter.name.push_back("Initialize start");
	my_counter.timestamp.push_back(tick_count::now());
	my_counter.index += 1;*/

	graph g;
	size_t mysize=atoi(argv[1]);
	int myblocks=atoi(argv[2]);
	size_t iterations = 20;
	size_t vis_frequency = 10;
	size_t vis_cycles = 2;
	size_t vis_offset = 0;
	size_t live_frequency = 2;
	size_t live_cycles = 10;
	size_t live_offset = 1;
	size_t neigh_frequency = 2;
	size_t neigh_cycles = 10;
	size_t neigh_offset = 1;
	double time_taken = 0.0;

	GoL = new GameOfLife;
	GoL->init(mysize,myblocks);
	broadcast_node<continue_msg> start(g);
	vector<continue_node<continue_msg> > sim_nodes;
	vector<continue_node<continue_msg> > visual_nodes;
	vector<continue_node<continue_msg> > live_nodes;
	vector<continue_node<continue_msg> > neigh_nodes;
	vector<continue_node<continue_msg> > update_nodes;
	myblocks = GoL->getblocks();

	for(size_t i=0;i<iterations;++i){
		for(size_t j=0;j<myblocks;++j){
			sim_nodes.push_back(continue_node<continue_msg>(g,sim_body(j)));
		}
		update_nodes.push_back(continue_node<continue_msg>(g,update_body()));
	}

	for(size_t i=0;i<vis_cycles;++i){
		for(size_t j=0;j<myblocks;++j){
			visual_nodes.push_back(continue_node<continue_msg>(g,vis_body(j)));
		}
	}
	for(size_t i=0;i<live_cycles;++i){
		for(size_t j=0;j<myblocks;++j){
			live_nodes.push_back(continue_node<continue_msg>(g,live_body(j)));
		}
	}
	for(size_t i=0;i<neigh_cycles;++i){
		for(size_t j=0;j<myblocks;++j){
			neigh_nodes.push_back(continue_node<continue_msg>(g,neighbour_body(j)));
		}
	}

	make_edge(start,sim_nodes[0]);
	for(size_t i=0;i<iterations;++i){
		for(size_t j=0;j<myblocks;++j){
			if(j==(myblocks-1)){
				continue;
			}
			make_edge(sim_nodes[(i*myblocks)+j],sim_nodes[(i*myblocks)+j+1]);
		}
		if(i == (iterations-1)){
			break;
		}
		make_edge(sim_nodes[(i*myblocks)+myblocks-1],update_nodes[i]);
		make_edge(update_nodes[i],sim_nodes[(i*myblocks)+myblocks]);
	}

	/*for(size_t i=0;i<vis_cycles;++i){
		for(size_t j=0;j<myblocks;++j){
			make_edge(sim_nodes[((i*vis_frequency)*(myblocks))+j+myblocks*vis_offset],visual_nodes[(i*myblocks)+j]);
			if(i==0 && j==0){
				continue;
			}
			make_edge(visual_nodes[(i*myblocks)+j-1],visual_nodes[(i*myblocks)+j]);
		}
	}*/
	for(size_t i=0;i<live_cycles;++i){
		for(int j=0;j<myblocks;++j){
			make_edge(sim_nodes[((i*live_frequency)*(myblocks))+j+myblocks*live_offset],live_nodes[(i*myblocks)+j]);
			if(block_sim_analytics){
				if(j<(myblocks-2)){
					make_edge(live_nodes[(i*myblocks)+j],sim_nodes[((i*live_frequency)*(myblocks))+j+myblocks*live_offset+2]);
				}
			}
			if(block_analytics){
				if(j==(myblocks-1)){
					if(i==(live_cycles-1)){
						break;
					}
					make_edge(live_nodes[(i*myblocks)+j],update_nodes[live_offset + (live_frequency)*(i)]);
				}
				else{
					make_edge(live_nodes[(i*myblocks)+j],live_nodes[(i*myblocks)+j+1]);
				}
			}
		}
	}
	for(size_t i=0;i<neigh_cycles;++i){
		for(int j=0;j<myblocks;++j){
			make_edge(sim_nodes[((i*neigh_frequency)*(myblocks))+j+myblocks*neigh_offset],neigh_nodes[(i*myblocks)+j]);
			if(block_sim_analytics){
				if(j<(myblocks-2)){
					make_edge(neigh_nodes[(i*myblocks)+j],sim_nodes[((i*neigh_frequency)*(myblocks))+j+myblocks*neigh_offset+2]);
				}
			}
			if(block_analytics){
				if(j==(myblocks-1)){
					if(i==(neigh_cycles-1)){
						break;
					}
					make_edge(neigh_nodes[(i*myblocks)+j],update_nodes[neigh_offset + (neigh_frequency)*(i)]);
				}
				else{
					make_edge(neigh_nodes[(i*myblocks)+j],neigh_nodes[(i*myblocks)+j+1]);
				}
			}
		}
	}

	tick_count t0 = tick_count::now();
	start.try_put(continue_msg());
	g.wait_for_all();
	tick_count t1 = tick_count::now();
	time_taken += (t1-t0).seconds();
	cout<<time_taken<<endl;

/*
	size_t th_id = 0;
	double val;
	ifstream oldfile("mytrace.trace",std::ios::in);
	ofstream myfile;
	myfile.open("traces/newtrace.trace",std::ofstream::out | std::ofstream::trunc);
	myfile<<oldfile.rdbuf();
	myfile<<"0 Program 0 P"<<std::endl;
	myfile<<"0 Thread P T"<<std::endl;
	myfile<<"1 ThreadState T S"<<std::endl;
	myfile<<"5 S AnalyticsTaskStart \"0.0 0.0 1.0\" ATS"<<std::endl;  //Blue
	myfile<<"5 S AnalyticsTaskEnd \"1.0 0.0 0.0\" ATE"<<std::endl;  //Red
	myfile<<"5 S AnalyticsRegionStart \"0.0 1.0 1.0\" ARS"<<std::endl;  //Cyan
	myfile<<"5 S AnalyticsRegionEnd \"1.0 0.0 0.0\" ARE"<<std::endl;  //Red
	myfile<<"5 S SimulationTaskStart \"0.0 1.0 0.0\" STS"<<std::endl; //Green  
	myfile<<"5 S SimulationTaskEnd \"1.0 0.0 0.0\" STE"<<std::endl; //Red
	myfile<<"5 S SimulationRegionStart \"0.0 1.0 0.5\" SRS"<<std::endl; //Light Green
	myfile<<"5 S SimulationRegionEnd \"1.0 0.0 0.0\" SRE"<<std::endl; //Red
	myfile<<"5 S InitStart \"1.0 1.0 1.0\" IS"<<std::endl; //White
	myfile<<"5 S InitEnd \"1.0 0.0 0.0\" IE"<<std::endl; //Red
	myfile<<"5 S JoinStart \"1.0 1.0 0.0\" JS"<<std::endl; //Yellow
	myfile<<"5 S JoinEnd \"1.0 0.0 0.0\" JE"<<std::endl; //Red
	myfile<<"7 0 ProgramExecuting P 0 TPP"<<std::endl;
	for(TLS::const_iterator i=MyCounter.begin();i!=MyCounter.end();++i){
		myfile<<"7 0 Thread"<<th_id<<"Executing T TPP Th"<<th_id<<std::endl;
		th_id++;
	}
	th_id = 0;
	for(TLS::const_iterator i=MyCounter.begin();i!=MyCounter.end();++i){
		for(size_t j=0;j<(i->index);++j){
			if(((i->name[j])).compare("Analytics start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"ARS"<<endl;
			}
			else if(((i->name[j])).compare("Analytics end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"ARE"<<endl;
			}
			else if(((i->name[j])).compare("Analytics Task start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"ATS"<<endl;
			}
			else if(((i->name[j])).compare("Analytics Task end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"ATE"<<endl;
			}
			else if(((i->name[j])).compare("Simulation start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"SRS"<<endl;
			}
			else if(((i->name[j])).compare("Simulation end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"SRE"<<endl;
			}
			else if(((i->name[j])).compare("Simulation Task start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"STS"<<endl;
			}
			else if(((i->name[j])).compare("Simulation Task end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"STE"<<endl;
			}
			else if(((i->name[j])).compare("Join start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"JS"<<endl;
			}	
			else if(((i->name[j])).compare("Join end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"JE"<<endl;
			}
			else if(((i->name[j])).compare("Initialize start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"IS"<<endl;
			}	
			else if(((i->name[j])).compare("Initialize end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"IE"<<endl;
			}					
		}
		th_id++;
	}
*/
}