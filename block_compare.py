import matplotlib.pyplot as plt
import pandas as pd 
from ggplot import *
import numpy as np 

filename = ["3k/out.txt"]
mydict = {}
mylist = []

for x in filename:
	fp = open(x,"r")
	lines = fp.readlines()
	for i in range(0,len(lines)):
		if("Time" in lines[i]):
			if("Sync" in lines[i]):
				if("Sync" in mydict):
					mydict["Sync"] += float(lines[i+1])
				else:
					mydict["Sync"] = float(lines[i+1])
			elif("Sim" in lines[i]):
				if("Sim" in mydict):
					mydict["Sim"] += float(lines[i+1])
				else:
					mydict["Sim"] = float(lines[i+1])
			elif("Memcpy" in lines[i]):
				if("Memcpy" in mydict):
					mydict["Memcpy"] += float(lines[i+1])
				else:
					mydict["Memcpy"] = float(lines[i+1])
			elif("DP" in lines[i]):
				if(("DP"+str(lines[i][-3:-1])) in mydict):
					mydict["DP"+str(lines[i][-3:-1])] += float(lines[i+1])
				else:
					mydict["DP"+str(lines[i][-3:-1])] = float(lines[i+1])

for x in mydict:
	mydict[x] = mydict[x]/len(filename);

for x in mydict:
	if("DP" in x):
		mylist.append({"Blocks":int(x[-2:]),"Time":mydict[x]})

dat = pd.DataFrame(mylist)

print(
	ggplot(dat,aes(x='Blocks',y='Time')) +\
	geom_line(color='red',linetype='solid',size='1') +\
	geom_point(color='black',size=10,shape=10,fill='red') +\
	geom_vline(color='red',x=[15,30],linetype='dashed') +\
	xlim(0,70) +\
 	#ylim(0,100) +\
	xlab("Blocks") +\
	ylab("Performance Values") +\
	ggtitle("Time")
 	)

