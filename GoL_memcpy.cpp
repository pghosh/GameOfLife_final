# define _BSD_SOURCE
# include <chrono>
# include <thread>
# include <sys/time.h>
# include <cstdio>
# include <iostream>
# include <vector>
# include <fstream>
# include <limits>
# include <iomanip>
# include "tbb/parallel_for.h"
# include "tbb/parallel_reduce.h"
# include "tbb/blocked_range2d.h"
# include "tbb/blocked_range.h"
# include "tbb/tick_count.h"
# include "tbb/flow_graph.h"
# include "tbb/enumerable_thread_specific.h"
# include "tbb/task_scheduler_init.h"

using namespace std;
using namespace tbb;
using namespace tbb::flow;

/*
Color Codes:
	Analytics: Cyan->Blue
	Simulation: Light Green -> Green
	Initialization: White
	Joins: Yellow
	Idle: Red
*/

/*struct Traces{
	size_t index;
	tick_count start;
	vector<tick_count> timestamp;
	vector<string> name;
	Traces(size_t x,tick_count y):index(x),start(y){}
};*/

/*typedef enumerable_thread_specific<Traces> TLS;
TLS MyCounter(Traces(0,tick_count::now()));*/

class Alive{
	int** arr;
	size_t size;
public:
	double my_sum;
	void operator()(const blocked_range<size_t>& r){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics Task start");
		my_counter.timestamp.push_back(tick_count::now());*/
		int** a = arr;
		double sum = my_sum;
		for(size_t i=r.begin();i<r.end();++i){
			for(size_t j=1;j<(size-1);++j){
				if(a[i][j]==1){
					sum ++;
				} 
			}
		}
		my_sum = sum;
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics Task end");
		my_counter.index += 2;*/
	}

	Alive(Alive& x,split):arr(x.arr),size(x.size),my_sum(0){}

	void join(const Alive& y){my_sum += y.my_sum;}

	Alive(int** a,size_t b):arr(a),size(b),my_sum(0){}
};

class MyNeighbourhood{
	int** arr;
	size_t size;
	int myNeighbours(int xpos,int ypos,int** myarr){
		return (myarr[xpos+1][ypos] + myarr[xpos][ypos+1] + myarr[xpos-1][ypos] + myarr[xpos][ypos-1] + myarr[xpos+1][ypos+1] + myarr[xpos-1][ypos-1] + myarr[xpos+1][ypos-1] + myarr[xpos-1][ypos+1]);
	}
public:
	int tarr[9];
	void operator()(const blocked_range<size_t>& r){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics Task start");
		my_counter.timestamp.push_back(tick_count::now());*/
		int** a = arr;
		for(size_t i=r.begin();i<r.end();++i){
			for(size_t j=1;j<(size-1);++j){
				tarr[myNeighbours(i,j,a)] += 1;
			}
		}
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics Task end");
		my_counter.index += 2;*/
	}

	MyNeighbourhood(MyNeighbourhood& x,split):arr(x.arr),size(x.size){
		for(int i=0;i<=9;++i){
			tarr[i]=0;
		}
	}

	void join(const MyNeighbourhood& y){
		for(int i=0;i<=9;++i){
			tarr[i] += y.tarr[i];
		}
	}

	MyNeighbourhood(int** a,size_t x):arr(a),size(x){
		for(int i=0;i<=9;++i){
			tarr[i]=0;
		}
	}
};

struct GameOfLife{
private:
	int** oldbuf;
	int** newbuf;
	int** anybuf;
	size_t size;
	ofstream visfile;

	void ParticleSimulate(int xpos,int ypos){
		int temp = this->newbuf[xpos][ypos];
		if(this->oldbuf[xpos][ypos] == 1){
			if(temp<2 || temp>3){
				this->newbuf[xpos][ypos]=0;
			}
			else{
				this->newbuf[xpos][ypos]=1;
			}	
		}
		else{
			if(temp==3){
				this->newbuf[xpos][ypos]=1;
			}
			else{
				this->newbuf[xpos][ypos]=0;
			}
		}
	}

	void ParticleUpdate(int xpos, int ypos, int track){
		switch(track){
			case 0:
				this->newbuf[xpos][ypos] = this->oldbuf[xpos-1][ypos-1];
				break;
			case 1:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos-1][ypos];
				break;
			case 2:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos-1][ypos+1];
				break;
			case 3:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos][ypos-1];
				break;
			case 4:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos][ypos+1];
				break;
			case 5:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos+1][ypos-1];
				break;
			case 6:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos+1][ypos];
				break;
			case 7:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos+1][ypos+1];
				break;
			default:
				printf("Error passing track to Update function\n");
		}
	}
public:
	GameOfLife(){}

	void init(size_t mysize){
		srand(time(NULL));
		this->size = mysize;
		visfile.open("visual.txt",std::ios::out | std::ios::trunc);
		oldbuf = new int*[mysize];
		newbuf = new int*[mysize];
		anybuf = new int*[mysize];
		for(size_t i=0;i<mysize;++i){
			oldbuf[i]=new int[mysize];
			newbuf[i]=new int[mysize];
			anybuf[i]=new int[mysize];
			for(size_t j=0;j<mysize;++j){
				oldbuf[i][j]=rand()%2;
				newbuf[i][j]=rand()%2;
				anybuf[i][j]=rand()%2;
			}
		}
	}

	void simulator(int grain = 1){
		affinity_partitioner ap;
		for(int track = 0;track < 8;++track){
			parallel_for(blocked_range<size_t>(1,(this->size)-1,grain),
				[=](const blocked_range<size_t>& r){
					/*TLS::reference my_counter = MyCounter.local();
					my_counter.name.push_back("Simulation Task start");
					my_counter.timestamp.push_back(tick_count::now());*/
					for(size_t i = r.begin(); i < r.end(); ++i){
						for(size_t j = 1;j < (this->size)-1 ;++j){
							ParticleUpdate(i,j,track);
						}
					}
					/*my_counter.timestamp.push_back(tick_count::now());
					my_counter.name.push_back("Simulation Task end");
					my_counter.index += 2;*/
				}
			,ap);
		}
		parallel_for(blocked_range<size_t>(1,(this->size)-1,grain),
			[=](const blocked_range<size_t>& r){
				/*TLS::reference my_counter = MyCounter.local();
				my_counter.name.push_back("Simulation Task start");
				my_counter.timestamp.push_back(tick_count::now());*/
				for(size_t i=r.begin();i<r.end();++i){
					for(size_t j=1;j<((this->size)-1);++j){
						ParticleSimulate(i,j);
					}
				}
				/*my_counter.timestamp.push_back(tick_count::now());
				my_counter.name.push_back("Simulation Task end");
				my_counter.index += 2;*/
			}
		,ap);
	}

	void memcpy(){
		for(size_t i = 1;i < (this->size)-1;++i){
			for(size_t j = 1;j < (this->size)-1;++j){
				anybuf[i][j] = newbuf[i][j];
			}
		}
	}

	void update(){
		swap(oldbuf,newbuf);
	}

	void visualize(){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics start");
		my_counter.timestamp.push_back(tick_count::now());*/
		for(size_t i=1;i<(this->size)-1;++i){
			for(size_t j=1;j<(this->size-1);++j){
				if(this->anybuf[i][j]==1){
					visfile<<".";
				}
				else{
					visfile<<" ";
				}
			}
			visfile<<endl;
		}
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics end");
		my_counter.index += 2;*/
	}

	void live_cells(){
		affinity_partitioner ap;
		Alive lobj(this->anybuf,this->size);
		parallel_reduce(blocked_range<size_t>(1,(this->size)-1),lobj,ap);
	}

	void neighbourhood(){
		affinity_partitioner ap;
		MyNeighbourhood nobj(this->anybuf,this->size);
		parallel_reduce(blocked_range<size_t>(1,(this->size)-1),nobj,ap);
	}
};

GameOfLife* GoL;

struct sim_body{
	sim_body(){}
	void operator()(continue_msg)const{
		GoL->simulator();
	}
};

struct vis_body{
	vis_body(){}
	void operator()(continue_msg)const{
		GoL->visualize();
	}
};

struct live_body{
	live_body(){}
	void operator()(continue_msg)const{
		GoL->live_cells();
	}
};

struct neighbour_body{
	neighbour_body(){}
	void operator()(continue_msg)const{
		GoL->neighbourhood();
	}
};

struct update_body{
	update_body(){}
	void operator()(continue_msg)const{
		GoL->update();
	}
};

struct memcpy_body{
	memcpy_body(){}
	void operator()(continue_msg)const{
		GoL->memcpy();
	}
};

int main(int argc,char* argv[]){
	/*TLS::reference my_counter = MyCounter.local();
	my_counter.name.push_back("Initialize start");
	my_counter.timestamp.push_back(tick_count::now());
	my_counter.index += 1;*/

	graph g;
	size_t mysize=atoi(argv[1]);
	size_t iterations = 10;
	size_t vis_frequency = 5;
	size_t vis_cycles = 2;
	size_t vis_offset = 0;
	size_t live_frequency = 2;
	size_t live_cycles = 5;
	size_t live_offset = 1;
	size_t neigh_frequency = 2;
	size_t neigh_cycles = 5;
	size_t neigh_offset = 1;
	double time_taken = 0.0;
	size_t trials = 3;

	GoL = new GameOfLife;
	GoL->init(mysize);
	broadcast_node<continue_msg> start(g);
	vector<continue_node<continue_msg> > sim_nodes;
	vector<continue_node<continue_msg> > visual_nodes;
	vector<continue_node<continue_msg> > live_nodes;
	vector<continue_node<continue_msg> > neigh_nodes;
	vector<continue_node<continue_msg> > update_nodes;
	vector<continue_node<continue_msg> > memcpy_nodes;

	for(size_t i=0;i<iterations;++i){
		sim_nodes.push_back(continue_node<continue_msg>(g,sim_body()));
		update_nodes.push_back(continue_node<continue_msg>(g,update_body()));
		memcpy_nodes.push_back(continue_node<continue_msg>(g,memcpy_body()));
	}
	for(size_t i=0;i<vis_cycles;++i){
		visual_nodes.push_back(continue_node<continue_msg>(g,vis_body()));
	}
	for(size_t i=0;i<live_cycles;++i){
		live_nodes.push_back(continue_node<continue_msg>(g,live_body()));
	}
	for(size_t i=0;i<neigh_cycles;++i){
		neigh_nodes.push_back(continue_node<continue_msg>(g,neighbour_body()));
	}


	make_edge(start,sim_nodes[0]);
	for(size_t i=0;i<iterations;++i){
		make_edge(sim_nodes[i],memcpy_nodes[i]);
		if(i == (iterations-1)){
			break;
		}
		make_edge(sim_nodes[i],update_nodes[i]);
		make_edge(update_nodes[i],sim_nodes[i+1]);
		make_edge(memcpy_nodes[i],sim_nodes[i+1]);
	}
	for(size_t i=0;i<vis_cycles;++i){
		make_edge(memcpy_nodes[vis_offset+(vis_frequency*i)],visual_nodes[i]);
		if(i == (vis_cycles-1)){
			break;
		}
		make_edge(visual_nodes[i],visual_nodes[i+1]);
	}
	for(size_t i=0;i<live_cycles;++i){
		make_edge(memcpy_nodes[live_offset+(live_frequency*i)],live_nodes[i]);
	}
	for(size_t i=0;i<neigh_cycles;++i){
		make_edge(memcpy_nodes[neigh_offset+(neigh_frequency*i)],neigh_nodes[i]);
	}

	for(size_t i = 0;i < trials;++i){
		tick_count t0 = tick_count::now();
		start.try_put(continue_msg());
		g.wait_for_all();
		tick_count t1 = tick_count::now();
		time_taken += (t1-t0).seconds();
	}
	cout<<time_taken/trials<<endl;



}