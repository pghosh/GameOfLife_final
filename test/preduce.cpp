# define _BSD_SOURCE
# include <chrono>
# include <thread>
# include <sys/time.h>
# include <cstdio>
# include <iostream>
# include <vector>
# include <fstream>
# include <limits>
# include <iomanip>
# include "tbb/parallel_for.h"
# include "tbb/parallel_reduce.h"
# include "tbb/blocked_range2d.h"
# include "tbb/blocked_range.h"
# include "tbb/tick_count.h"
# include "tbb/flow_graph.h"
# include "tbb/enumerable_thread_specific.h"
# include "tbb/task_scheduler_init.h"

using namespace tbb;
using namespace std;

struct Sum {
    float value;
    Sum(int myval) : value(myval) {}
    Sum( Sum& s, split ) {value = 0;}
    void operator()( const blocked_range<float*>& r ) {
        float temp = value;
        for( float* a=r.begin(); a!=r.end(); ++a ) {
            temp += *a;
        }
        value = temp;
    }
    void join( Sum& rhs ) {value += rhs.value;}
};


int main(){
	int myval = 10;
	int size = 1000;
	float mysum=0;
	float array[size];
	for(int i=0;i<size;++i){
		array[i]=i;
		mysum += i;
	}
	cout<<"Serial Value:"<<mysum<<endl;

	Sum total(myval);
    parallel_reduce( blocked_range<float*>(array,array+size),total );
    cout<<"Parallel Value:"<<total.value<<endl;
}