#!/bin/bash

lscpu > data.txt
. ../../../tbb/tbb43_20150611oss/bin/tbbvars.sh intel64

g++ GoL_graph_dp.cpp -ltbb -std=c++11
for number in 1 #1 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80
do 
	echo "DP_Time" $number >> out.txt
	hwloc-bind --physical socket:3.core:3 perf stat -r 3 -o out_$number.txt -e r412e,r4f2e,cache-misses,cache-references,LLC-load-misses,LLC-load,LLC-store-misses ./a.out $1 $number >> out.txt
	echo "DONE"
done

exit 0
