# define _BSD_SOURCE
# include <chrono>
# include <thread>
# include <sys/time.h>
# include <cstdio>
# include <iostream>
# include <vector>
# include <fstream>
# include <limits>
# include <iomanip>
# include "tbb/parallel_for.h"
# include "tbb/parallel_reduce.h"
# include "tbb/blocked_range2d.h"
# include "tbb/blocked_range.h"
# include "tbb/tick_count.h"
# include "tbb/flow_graph.h"
# include "tbb/enumerable_thread_specific.h"
# include "tbb/task_scheduler_init.h"

using namespace std;
using namespace tbb;
using namespace tbb::flow;

/*
Color Codes:
	Analytics: Cyan->Blue
	Simulation: Light Green -> Green
	Initialization: White
	Joins: Yellow
	Idle: Red
*/

/*struct Traces{
	size_t index;
	tick_count start;
	vector<tick_count> timestamp;
	vector<string> name;
	Traces(size_t x,tick_count y):index(x),start(y){}
};*/

/*typedef enumerable_thread_specific<Traces> TLS;
TLS MyCounter(Traces(0,tick_count::now()));*/

class Alive{
	int** arr;
	size_t size;
public:
	double my_sum;
	void operator()(const blocked_range<size_t>& r){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics Task start");
		my_counter.timestamp.push_back(tick_count::now());*/
		int** a = arr;
		double sum = my_sum;
		for(size_t i=r.begin();i<r.end();++i){
			for(size_t j=1;j<(size-1);++j){
				if(a[i][j]==1){
					sum ++;
				} 
			}
		}
		my_sum = sum;
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics Task end");
		my_counter.index += 2;*/
	}

	Alive(Alive& x,split):arr(x.arr),size(x.size),my_sum(0){}

	void join(const Alive& y){my_sum += y.my_sum;}

	Alive(int** a,size_t b):arr(a),size(b),my_sum(0){}
};

class MyNeighbourhood{
	int** arr;
	size_t size;
	int myNeighbours(int xpos,int ypos,int** myarr){
		return (myarr[xpos+1][ypos] + myarr[xpos][ypos+1] + myarr[xpos-1][ypos] + myarr[xpos][ypos-1] + myarr[xpos+1][ypos+1] + myarr[xpos-1][ypos-1] + myarr[xpos+1][ypos-1] + myarr[xpos-1][ypos+1]);
	}
public:
	int tarr[9];
	void operator()(const blocked_range<size_t>& r){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics Task start");
		my_counter.timestamp.push_back(tick_count::now());*/
		int** a = arr;
		for(size_t i=r.begin();i<r.end();++i){
			for(size_t j=1;j<(size-1);++j){
				tarr[myNeighbours(i,j,a)] += 1;
			}
		}
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics Task end");
		my_counter.index += 2;*/
	}

	MyNeighbourhood(MyNeighbourhood& x,split):arr(x.arr),size(x.size){
		for(int i=0;i<=9;++i){
			tarr[i]=0;
		}
	}

	void join(const MyNeighbourhood& y){
		for(int i=0;i<=9;++i){
			tarr[i] += y.tarr[i];
		}
	}

	MyNeighbourhood(int** a,size_t x):arr(a),size(x){
		for(int i=0;i<=9;++i){
			tarr[i]=0;
		}
	}
};

struct GameOfLife{
private:
	int** oldbuf;
	int** newbuf;
	size_t size;
	size_t blocks;
	vector<pair<size_t,size_t> > block_points;
	ofstream visfile;
	vector<pair<size_t,size_t> > trackmap;

	void ParticleSimulate(int xpos,int ypos){
		int temp = this->newbuf[xpos][ypos];
		if(this->oldbuf[xpos][ypos] == 1){
			if(temp<2 || temp>3){
				this->newbuf[xpos][ypos]=0;
			}
			else{
				this->newbuf[xpos][ypos]=1;
			}	
		}
		else{
			if(temp==3){
				this->newbuf[xpos][ypos]=1;
			}
			else{
				this->newbuf[xpos][ypos]=0;
			}
		}
	}

	void ParticleUpdate(int xpos, int ypos, int track){
		this->newbuf[xpos][ypos] += this->oldbuf[xpos+(this->trackmap[track].first)][ypos+(this->trackmap[track].second)];
		/*switch(track){
			case 0:
				this->newbuf[xpos][ypos] = this->oldbuf[xpos-1][ypos-1];
				break;
			case 1:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos-1][ypos];
				break;
			case 2:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos-1][ypos+1];
				break;
			case 3:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos][ypos-1];
				break;
			case 4:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos][ypos+1];
				break;
			case 5:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos+1][ypos-1];
				break;
			case 6:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos+1][ypos];
				break;
			case 7:
				this->newbuf[xpos][ypos] += this->oldbuf[xpos+1][ypos+1];
				break;
			default:
				printf("Error passing track to Update function\n");
		}*/
	}
public:
	GameOfLife(){}

	void init(size_t mysize, size_t myblocks){
		srand(time(NULL));
		this->size = mysize;
		this->blocks = myblocks;
		visfile.open("visual.txt",std::ios::out | std::ios::trunc);
		oldbuf = new int*[mysize];
		newbuf = new int*[mysize];
		for(size_t i=0;i<mysize;++i){
			oldbuf[i]=new int[mysize];
			newbuf[i]=new int[mysize];
			for(size_t j=0;j<mysize;++j){
				oldbuf[i][j]=rand()%2;
				newbuf[i][j]=rand()%2;
			}
		}
		size_t tstart,tend;
		//Fix this
		for(size_t j = 0;j<myblocks;++j){
			tstart = ((mysize/myblocks)*j)+1;
			tend = ((mysize/myblocks)*(j+1))+1;
			if(j==(myblocks-1)){
				tend = mysize-1;
			}
			block_points.push_back(make_pair(tstart,tend));
		}
		for(size_t i =0;i<8;++i){
			if(i==0){
				trackmap.push_back(make_pair(-1,-1));
			}
			else if(i==1){
				trackmap.push_back(make_pair(-1,0));
			}
			else if(i==2){
				trackmap.push_back(make_pair(-1,1));
			}
			else if(i==3){
				trackmap.push_back(make_pair(0,-1));
			}
			else if(i==4){
				trackmap.push_back(make_pair(0,1));
			}
			else if(i==5){
				trackmap.push_back(make_pair(1,-1));
			}
			else if(i==6){
				trackmap.push_back(make_pair(1,0));
			}
			else if(i==7){
				trackmap.push_back(make_pair(1,1));
			}
		}
	}

	void block_simulator(size_t block_id, int grain = 1){
		affinity_partitioner ap;
		for(int track = 0;track < 8;++track){
			parallel_for(blocked_range<size_t>(this->block_points[block_id].first,this->block_points[block_id].second,grain),
				[=](const blocked_range<size_t>& r){
					/*TLS::reference my_counter = MyCounter.local();
					my_counter.name.push_back("Simulation Task start");
					my_counter.timestamp.push_back(tick_count::now());*/
					for(size_t i = r.begin(); i < r.end(); ++i){
						for(size_t j = 1;j < (this->size)-1 ;++j){
							if(track==0){
								this->newbuf[i][j] = 0;
							}
							ParticleUpdate(i,j,track);
						}
					}
					/*my_counter.timestamp.push_back(tick_count::now());
					my_counter.name.push_back("Simulation Task end");
					my_counter.index += 2;*/
				}
			,ap);
		}
		parallel_for(blocked_range<size_t>(this->block_points[block_id].first,this->block_points[block_id].second,grain),
			[=](const blocked_range<size_t>& r){
				/*TLS::reference my_counter = MyCounter.local();
				my_counter.name.push_back("Simulation Task start");
				my_counter.timestamp.push_back(tick_count::now());*/
				for(size_t i=r.begin();i<r.end();++i){
					for(size_t j=1;j<((this->size)-1);++j){
						ParticleSimulate(i,j);
					}
				}
				/*my_counter.timestamp.push_back(tick_count::now());
				my_counter.name.push_back("Simulation Task end");
				my_counter.index += 2;*/
			}
		,ap);
	}

	void update(){
		swap(oldbuf,newbuf);
	}

	void block_visualize(size_t block_id){
		/*TLS::reference my_counter = MyCounter.local();
		my_counter.name.push_back("Analytics start");
		my_counter.timestamp.push_back(tick_count::now());*/
		for(size_t i=(this->block_points[block_id].first);i<(this->block_points[block_id].second);++i){
			for(size_t j=1;j<(this->size-1);++j){
				if(this->oldbuf[i][j]==1){
					visfile<<".";
				}
				else{
					visfile<<" ";
				}
			}
			visfile<<endl;
		}
		/*my_counter.timestamp.push_back(tick_count::now());
		my_counter.name.push_back("Analytics end");
		my_counter.index += 2;*/
	}

	void live_cells(size_t block_id){
		affinity_partitioner ap;
		Alive lobj(this->newbuf,this->size);
		parallel_reduce(blocked_range<size_t>(this->block_points[block_id].first,this->block_points[block_id].second),lobj,ap);
	}

	void neighbourhood(size_t block_id){
		affinity_partitioner ap;
		MyNeighbourhood nobj(this->newbuf,this->size);
		parallel_reduce(blocked_range<size_t>(this->block_points[block_id].first,this->block_points[block_id].second),nobj,ap);
	}
};

GameOfLife* GoL;

struct sim_body{
	size_t id;
	sim_body(size_t x):id(x){}
	void operator()(continue_msg)const{
		GoL->block_simulator(id);
	}
};

struct vis_body{
	size_t id;
	vis_body(size_t x):id(x){}
	void operator()(continue_msg)const{
		GoL->block_visualize(id);
		//cout<<id<<endl;
	}
};

struct live_body{
	size_t id;
	live_body(size_t x):id(x){}
	void operator()(continue_msg)const{
		GoL->live_cells(id);
		//cout<<id<<endl;
	}
};

struct neighbour_body{
	size_t id;
	neighbour_body(size_t x):id(x){}
	void operator()(continue_msg)const{
		GoL->neighbourhood(id);
		//cout<<id<<endl;
	}
};

struct update_body{
	update_body(){}
	void operator()(continue_msg)const{
		GoL->update();
	}
};

int main(int argc,char* argv[]){
	/*TLS::reference my_counter = MyCounter.local();
	my_counter.name.push_back("Initialize start");
	my_counter.timestamp.push_back(tick_count::now());
	my_counter.index += 1;*/

	graph g;
	size_t mysize=atoi(argv[1]);
	size_t myblocks=atoi(argv[2]);
	size_t iterations = 10;
	size_t vis_frequency = 5;
	size_t vis_cycles = 2;
	size_t vis_offset = 0;
	size_t live_frequency = 2;
	size_t live_cycles = 5;
	size_t live_offset = 1;
	size_t neigh_frequency = 2;
	size_t neigh_cycles = 5;
	size_t neigh_offset = 1;
	size_t trials = 3;
	double time_taken = 0.0;

	GoL = new GameOfLife;
	GoL->init(mysize,myblocks);
	//cout<<"Memory Allocation complete"<<":"<<myblocks<<endl;
	broadcast_node<continue_msg> start(g);
	vector<continue_node<continue_msg> > sim_nodes;
	vector<continue_node<continue_msg> > visual_nodes;
	vector<continue_node<continue_msg> > live_nodes;
	vector<continue_node<continue_msg> > neigh_nodes;
	vector<continue_node<continue_msg> > update_nodes;

	for(size_t i=0;i<iterations;++i){
		for(size_t j=0;j<myblocks;++j){
			sim_nodes.push_back(continue_node<continue_msg>(g,sim_body(j)));
		}
		update_nodes.push_back(continue_node<continue_msg>(g,update_body()));
	}
	for(size_t i=0;i<vis_cycles;++i){
		for(size_t j=0;j<myblocks;++j){
			visual_nodes.push_back(continue_node<continue_msg>(g,vis_body(j)));
		}
	}
	for(size_t i=0;i<live_cycles;++i){
		for(size_t j=0;j<myblocks;++j){
			live_nodes.push_back(continue_node<continue_msg>(g,live_body(j)));
		}
	}
	for(size_t i=0;i<neigh_cycles;++i){
		for(size_t j=0;j<myblocks;++j){
			neigh_nodes.push_back(continue_node<continue_msg>(g,neighbour_body(j)));
		}
	}

	make_edge(start,sim_nodes[0]);
	for(size_t i=0;i<iterations;++i){
		for(size_t j=0;j<myblocks;++j){
			if(j==(myblocks-1)){
				continue;
			}
			make_edge(sim_nodes[(i*myblocks)+j],sim_nodes[(i*myblocks)+j+1]);
		}
		if(i == (iterations-1)){
			break;
		}
		make_edge(sim_nodes[(i*myblocks)+myblocks-1],update_nodes[i]);
		make_edge(update_nodes[i],sim_nodes[(i*myblocks)+myblocks]);
	}
	for(size_t i=0;i<vis_cycles;++i){
		for(size_t j=0;j<myblocks;++j){
			make_edge(sim_nodes[((i*vis_frequency)*(myblocks))+j+myblocks*vis_offset],visual_nodes[(i*myblocks)+j]);
			if(i==0 && j==0){
				continue;
			}
			make_edge(visual_nodes[(i*myblocks)+j-1],visual_nodes[(i*myblocks)+j]);
		}
	}
	for(size_t i=0;i<live_cycles;++i){
		for(size_t j=0;j<myblocks;++j){
			make_edge(sim_nodes[((i*live_frequency)*(myblocks))+j+myblocks*live_offset],live_nodes[(i*myblocks)+j]);
			//cout<<(((i)*live_frequency)*(myblocks))+j+myblocks*live_offset<<" "<<(i*myblocks)+j<<endl;
		}
	}
	for(size_t i=0;i<neigh_cycles;++i){
		for(size_t j=0;j<myblocks;++j){
			make_edge(sim_nodes[((i*neigh_frequency)*(myblocks))+j+myblocks*neigh_offset],neigh_nodes[(i*myblocks)+j]);
			//cout<<((i*neigh_frequency)*(myblocks))+j+myblocks*neigh_offset<<" "<<(i*myblocks)+j<<endl;
		}
	}

/*
	continue_node<continue_msg> s11(g,sim_body(0));
	continue_node<continue_msg> s12(g,sim_body(1));
	continue_node<continue_msg> s13(g,sim_body(2));
	continue_node<continue_msg> s14(g,sim_body(3));
	continue_node<continue_msg> s15(g,sim_body(4));

	continue_node<continue_msg> s21(g,sim_body(0));
	continue_node<continue_msg> s22(g,sim_body(1));
	continue_node<continue_msg> s23(g,sim_body(2));
	continue_node<continue_msg> s24(g,sim_body(3));
	continue_node<continue_msg> s25(g,sim_body(4));

	continue_node<continue_msg> s31(g,sim_body(0));
	continue_node<continue_msg> s32(g,sim_body(1));
	continue_node<continue_msg> s33(g,sim_body(2));
	continue_node<continue_msg> s34(g,sim_body(3));
	continue_node<continue_msg> s35(g,sim_body(4));

	continue_node<continue_msg> s41(g,sim_body(0));
	continue_node<continue_msg> s42(g,sim_body(1));
	continue_node<continue_msg> s43(g,sim_body(2));
	continue_node<continue_msg> s44(g,sim_body(3));
	continue_node<continue_msg> s45(g,sim_body(4));

	continue_node<continue_msg> s51(g,sim_body(0));
	continue_node<continue_msg> s52(g,sim_body(1));
	continue_node<continue_msg> s53(g,sim_body(2));
	continue_node<continue_msg> s54(g,sim_body(3));
	continue_node<continue_msg> s55(g,sim_body(4));

	continue_node<continue_msg> a21(g,live_body(0));
	continue_node<continue_msg> a22(g,live_body(1));
	continue_node<continue_msg> a23(g,live_body(2));
	continue_node<continue_msg> a24(g,live_body(3));
	continue_node<continue_msg> a25(g,live_body(4));

	continue_node<continue_msg> a11(g,vis_body(0));
	continue_node<continue_msg> a12(g,vis_body(1));
	continue_node<continue_msg> a13(g,vis_body(2));
	continue_node<continue_msg> a14(g,vis_body(3));
	continue_node<continue_msg> a15(g,vis_body(4));

	continue_node<continue_msg> a31(g,neighbour_body(0));
	continue_node<continue_msg> a32(g,neighbour_body(1));
	continue_node<continue_msg> a33(g,neighbour_body(2));
	continue_node<continue_msg> a34(g,neighbour_body(3));
	continue_node<continue_msg> a35(g,neighbour_body(4));

	make_edge(start,s11);
	make_edge(s11,s12);
	make_edge(s12,s13);
	make_edge(s13,s14);
	make_edge(s14,s15);
	make_edge(s15,s21);

	make_edge(s21,s22);
	make_edge(s22,s23);
	make_edge(s23,s24);
	make_edge(s24,s25);
	make_edge(s25,s31);

	make_edge(s31,s32);
	make_edge(s32,s33);
	make_edge(s33,s34);
	make_edge(s34,s35);
	make_edge(s35,s41);

	make_edge(s41,s42);
	make_edge(s42,s43);
	make_edge(s43,s44);
	make_edge(s44,s45);
	make_edge(s45,s51);

	make_edge(s51,s52);
	make_edge(s52,s53);
	make_edge(s53,s54);
	make_edge(s54,s55);

	make_edge(s11,a11);
	make_edge(s12,a12);
	make_edge(s13,a13);
	make_edge(s14,a14);
	make_edge(s15,a15);

	make_edge(a11,a12);
	make_edge(a12,a13);
	make_edge(a13,a14);
	make_edge(a14,a15);

	make_edge(s21,a21);
	make_edge(s22,a22);
	make_edge(s23,a23);
	make_edge(s24,a24);
	make_edge(s25,a25);

	make_edge(s31,a31);
	make_edge(s32,a32);
	make_edge(s33,a33);
	make_edge(s34,a34);
	make_edge(s35,a35);
*/
	for(size_t i = 0;i < trials; ++i){
		tick_count t0 = tick_count::now();
		start.try_put(continue_msg());
		g.wait_for_all();
		tick_count t1 = tick_count::now();
		time_taken += (t1-t0).seconds();
	}
	cout<<time_taken/trials<<endl;



/*
	size_t th_id = 0;
	double val;
	ifstream oldfile("mytrace.trace",std::ios::in);
	ofstream myfile;
	myfile.open("traces/newtrace.trace",std::ofstream::out | std::ofstream::trunc);
	myfile<<oldfile.rdbuf();
	myfile<<"0 Program 0 P"<<std::endl;
	myfile<<"0 Thread P T"<<std::endl;
	myfile<<"1 ThreadState T S"<<std::endl;
	myfile<<"5 S AnalyticsTaskStart \"0.0 0.0 1.0\" ATS"<<std::endl;  //Blue
	myfile<<"5 S AnalyticsTaskEnd \"1.0 0.0 0.0\" ATE"<<std::endl;  //Red
	myfile<<"5 S AnalyticsRegionStart \"0.0 1.0 1.0\" ARS"<<std::endl;  //Cyan
	myfile<<"5 S AnalyticsRegionEnd \"1.0 0.0 0.0\" ARE"<<std::endl;  //Red
	myfile<<"5 S SimulationTaskStart \"0.0 1.0 0.0\" STS"<<std::endl; //Green  
	myfile<<"5 S SimulationTaskEnd \"1.0 0.0 0.0\" STE"<<std::endl; //Red
	myfile<<"5 S SimulationRegionStart \"0.0 1.0 0.5\" SRS"<<std::endl; //Light Green
	myfile<<"5 S SimulationRegionEnd \"1.0 0.0 0.0\" SRE"<<std::endl; //Red
	myfile<<"5 S InitStart \"1.0 1.0 1.0\" IS"<<std::endl; //White
	myfile<<"5 S InitEnd \"1.0 0.0 0.0\" IE"<<std::endl; //Red
	myfile<<"5 S JoinStart \"1.0 1.0 0.0\" JS"<<std::endl; //Yellow
	myfile<<"5 S JoinEnd \"1.0 0.0 0.0\" JE"<<std::endl; //Red
	myfile<<"7 0 ProgramExecuting P 0 TPP"<<std::endl;
	for(TLS::const_iterator i=MyCounter.begin();i!=MyCounter.end();++i){
		myfile<<"7 0 Thread"<<th_id<<"Executing T TPP Th"<<th_id<<std::endl;
		th_id++;
	}
	th_id = 0;
	for(TLS::const_iterator i=MyCounter.begin();i!=MyCounter.end();++i){
		for(size_t j=0;j<(i->index);++j){
			if(((i->name[j])).compare("Analytics start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"ARS"<<endl;
			}
			else if(((i->name[j])).compare("Analytics end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"ARE"<<endl;
			}
			else if(((i->name[j])).compare("Analytics Task start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"ATS"<<endl;
			}
			else if(((i->name[j])).compare("Analytics Task end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"ATE"<<endl;
			}
			else if(((i->name[j])).compare("Simulation start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"SRS"<<endl;
			}
			else if(((i->name[j])).compare("Simulation end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"SRE"<<endl;
			}
			else if(((i->name[j])).compare("Simulation Task start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"STS"<<endl;
			}
			else if(((i->name[j])).compare("Simulation Task end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"STE"<<endl;
			}
			else if(((i->name[j])).compare("Join start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"JS"<<endl;
			}	
			else if(((i->name[j])).compare("Join end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"JE"<<endl;
			}
			else if(((i->name[j])).compare("Initialize start")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"IS"<<endl;
			}	
			else if(((i->name[j])).compare("Initialize end")==0){
				val = ((i->timestamp[j])-(i->start)).seconds();
				myfile<<"10 "<<std::fixed<<std::setprecision(10)<<val<<" "<<"S Th"<<th_id<<" "<<"IE"<<endl;
			}					
		}
		th_id++;
	}
*/
}