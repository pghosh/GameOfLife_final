import matplotlib.pyplot as plt
import pandas as pd 
from ggplot import *
import numpy as np 

filepath = "final_results/40k/out_"
blocks = [2,4,6,8,10,12,14,16,18,20]
mydict = {}
mylist = []

for x in blocks:
	fp = open(filepath+str(x)+".txt","r")
	mydict["Blocks"] = x
	lines = fp.readlines()
	for i in range(0,len(lines)):
		if("cache-misses" in lines[i]):
			temp = lines[i].split(" ")[4]
			ftemp =""
			for x in temp.split(","):
				ftemp += x
			mydict["Cache_misses"] = float(ftemp)
			mydict["Cache_miss_percentage"] = float(lines[i].split(" ")[-12])
		if("LLC-load-misses" in lines[i]):
			temp = lines[i].split(" ")[7]
			ftemp =""
			for x in temp.split(","):
				ftemp += x
			mydict["LLC_misses"] = float(ftemp)
			mydict["LLC_miss_percentage"] = float(lines[i].split(" ")[-10][:-1])
	mylist.append(mydict.copy())
	mydict.clear()

dat = pd.DataFrame(mylist)

print(
	ggplot(dat,aes(x='Blocks',y='LLC_miss_percentage')) +\
	geom_line(color='red',linetype='solid',size='1') +\
	geom_point(color='black',size=10,shape=10,fill='red') +\
	geom_vline(color='red',x=[15,30],linetype='dashed') +\
	xlim(0,70) +\
 	#ylim(0,100) +\
	xlab("Blocks") +\
	ylab("LLC_miss_percentage") +\
	ggtitle("Time")
 	)

