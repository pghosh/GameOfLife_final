# started on Mon May  7 08:16:37 2018


 Performance counter stats for './a.out 60002 11' (3 runs):

13,409,446,839,818      instructions              #    2.60  insns per cycle          ( +-  0.01% )  (42.86%)
 5,153,492,398,461      cpu-cycles                                                    ( +-  0.05% )  (57.14%)
       380,399,655      cache-misses              #   40.235 % of all cache refs      ( +-  0.42% )  (71.43%)
       945,446,665      cache-references                                              ( +-  1.12% )  (85.71%)
       215,080,919      LLC-load-misses           #   56.55% of all LL-cache hits     ( +-  0.65% )  (85.71%)
       380,357,386      LLC-loads                                                     ( +-  1.07% )  (85.72%)
       161,779,486      LLC-store-misses                                              ( +-  0.12% )  (28.57%)

     297.923799426 seconds time elapsed                                          ( +-  0.04% )

