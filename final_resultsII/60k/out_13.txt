# started on Mon May  7 08:46:26 2018


 Performance counter stats for './a.out 60002 13' (3 runs):

13,411,517,116,335      instructions              #    2.60  insns per cycle          ( +-  0.01% )  (42.86%)
 5,158,853,733,704      cpu-cycles                                                    ( +-  0.06% )  (57.14%)
       403,130,559      cache-misses              #   39.954 % of all cache refs      ( +-  0.37% )  (71.43%)
     1,008,976,573      cache-references                                              ( +-  1.18% )  (85.71%)
       229,354,711      LLC-load-misses           #   56.98% of all LL-cache hits     ( +-  0.53% )  (85.71%)
       402,549,124      LLC-loads                                                     ( +-  0.74% )  (85.71%)
       170,510,504      LLC-store-misses                                              ( +-  0.24% )  (28.57%)

     298.177807363 seconds time elapsed                                          ( +-  0.06% )

