# started on Mon May  7 09:01:20 2018


 Performance counter stats for './a.out 60002 14' (3 runs):

13,413,199,122,008      instructions              #    2.60  insns per cycle          ( +-  0.00% )  (42.85%)
 5,161,296,553,375      cpu-cycles                                                    ( +-  0.03% )  (57.14%)
       408,188,685      cache-misses              #   39.717 % of all cache refs      ( +-  0.16% )  (71.43%)
     1,027,740,607      cache-references                                              ( +-  0.99% )  (85.71%)
       233,012,212      LLC-load-misses           #   56.04% of all LL-cache hits     ( +-  0.09% )  (85.71%)
       415,826,612      LLC-loads                                                     ( +-  1.93% )  (85.71%)
       172,340,163      LLC-store-misses                                              ( +-  0.32% )  (28.57%)

     298.183236056 seconds time elapsed                                          ( +-  0.03% )

