# started on Mon May  7 01:33:37 2018


 Performance counter stats for './a.out 40002 3' (3 runs):

 5,957,356,560,215      instructions              #    2.61  insns per cycle          ( +-  0.01% )  (42.86%)
 2,283,437,651,443      cpu-cycles                                                    ( +-  0.02% )  (57.14%)
       131,451,817      cache-misses              #   43.199 % of all cache refs      ( +-  0.11% )  (71.43%)
       304,296,258      cache-references                                              ( +-  0.69% )  (85.71%)
        71,381,679      LLC-load-misses           #   52.57% of all LL-cache hits     ( +-  0.18% )  (85.71%)
       135,775,905      LLC-loads                                                     ( +-  1.85% )  (85.72%)
        59,447,270      LLC-store-misses                                              ( +-  0.15% )  (28.57%)

     132.128918867 seconds time elapsed                                          ( +-  0.01% )

