# started on Mon May  7 02:59:47 2018


 Performance counter stats for './a.out 40002 16' (3 runs):

 5,970,203,198,283      instructions              #    2.59  insns per cycle          ( +-  0.01% )  (42.86%)
 2,300,943,387,050      cpu-cycles                                                    ( +-  0.06% )  (57.15%)
       140,521,484      cache-misses              #   25.959 % of all cache refs      ( +-  0.62% )  (71.43%)
       541,327,132      cache-references                                              ( +-  1.21% )  (85.71%)
        77,091,887      LLC-load-misses           #   34.32% of all LL-cache hits     ( +-  0.79% )  (85.71%)
       224,633,103      LLC-loads                                                     ( +-  1.51% )  (85.72%)
        62,981,587      LLC-store-misses                                              ( +-  0.57% )  (28.57%)

     133.027434199 seconds time elapsed                                          ( +-  0.05% )

